## PROCESSAMENTO, LOGS E REDE
---
* verificar o status do container
`docker stats srv02`, para ver as configruações de hadrware da maquina.
`docker update debian -m 1000M --cpus 0.2`<br>

Criando um container com limite de hardware.
`docker run --name srvorion -dti -m 512M --cpus 0.2 debian`

#### programa stress ele server para stressa o host;
entre no container e instalar `apt-get install stress -y` como usar: `strees --cpu 1 --vm-bytes 128M 1 --vm-bytes 50m `

---
### docker info
tras informações do servdor docker: `docker info`, para verificar um log de um container especifico: `docker logs srv02`, e `docker container top debian` onde ver os processos do container.

### Rede network
`docker network ls` monstra informações de rede do docker.<br>
`docker network inspect bridge`, mostra todos os ip dos container.

## Criando um rede separada.
`docker network create minha-rede`, criando um containe nesta rede `docker run -dti --name srv --network minha-rede debian`, listando `docker inspect srv`, este caso este container esta isolado;<br>

Para remove 2 ou mais container ao mesmo tempo faça: `docker rm -f host01 host02 host03` e assim por adiante;
para remove a re de criada: `docker network rm minha-rede` 