### armazenamento de dados
---
criar um diretrio na raiz: `mkdir /data` e `mkdir/mysql`<br>
`docker run -e MYSQLROOT_PASSWORD=Senha123 --name mysql -d -p 3306:3306 --volume=/data/mysql:/var/lib/mysql
---
## Tipos mount
* Bind mounts
* Named
* docker file

Criando um bind mount no container
`docker run -dti --mount type=bind,src=/home/suporte/data/debian,dst=/data debian`

Acessando a maquina: `docker exec -ti modest_fermat bash` ao dar um `ls` voce ira ver o diretorio `data` que foi criando um volume.
para parar o container `docker stop nameHost`<br>

criando um volume agora somente leitura:
`docker run -dti --mount type=bind,src=/home/suporte/data/debian,dst=/data,ro --name srvedivan debian`

## Volume
lista volumes `docker volume ls` <br>
* Para criar um volume, um volume e como se fose um outro HD de forma virtual.
`docker volume create documentos`, para lista: `dockeer volume ls`

O diretorio padrao do colume que docjer ficar em: `/var/lib/docker/volumes` aqui estara todos os volumes.
`docker run -dti --mount type=volume,src=documentos,dst=/volume --name srv02 debian`

obs• Para excluir um volume nenhum dos container deve esta usando o volume.<br>
excluido um volume `docker volume rm documentos`

listando `docker volume ls`, para excluir um container sem da o stop faça: `docker rm -f srv02`<br>
para ecluir todos os volumes: `docker volume prune` que nao estao em uso.<br>
e para exluir todos os container `docker container prune` <br>